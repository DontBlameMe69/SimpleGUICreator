package dev.dontblameme.sgc.utils

import dev.dontblameme.kutilsapi.customcommand.CustomCommand
import dev.dontblameme.kutilsapi.inventory.InventoryBuilder
import dev.dontblameme.kutilsapi.inventory.InventoryItem
import dev.dontblameme.kutilsapi.itembuilder.ItemBuilder
import dev.dontblameme.sgc.main.SimpleGUICreator
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.NamespacedKey
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.enchantments.Enchantment
import org.bukkit.inventory.ItemFlag
import org.bukkit.persistence.PersistentDataType
import org.bukkit.plugin.java.JavaPlugin
import java.io.File
import java.util.*
import java.util.regex.Pattern

/**
 * Creator: DontBlameMe69
 * Date: Mar 11, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

class FileReader(private val instance: JavaPlugin) {

    private val manager = CustomInventoryManager()
    private val redirectPattern = Pattern.compile("\\{[A-Za-z0-9]+}")

    init {
        val config = YamlConfiguration.loadConfiguration(File("${instance.dataFolder.path}${System.getProperty("file.separator")}commands.yml"))

        for(key in config.getKeys(false)) {
            val configSec = config.getConfigurationSection(key)!!
            val folder = configSec.getString("inventories")!!

            createInventories(folder)
            CustomCommand(key, configSec.getString("permission")!!, configSec.getString("permissionMessage")!!) {
                it.player.openInventory(manager.getInventory(folder, "default")!!)
            }
        }

    }

    fun getManager(): CustomInventoryManager = manager

    fun createInventories(subDirectory: String) {
        val files = File("${instance.dataFolder.path}${System.getProperty("file.separator")}$subDirectory")
        val invs = HashMap<String, InventoryBuilder>()

        for(file in files.listFiles()!!) {
            val config = YamlConfiguration.loadConfiguration(file)
            val title = config.getString("title")
            val size = config.getInt("size")
            val invBuilder = InventoryBuilder(title!!, size)
            val itemSection = config.getConfigurationSection("items")

            if(itemSection != null) {
                val topKeys = itemSection.getKeys(false)

                for(key in topKeys) {

                    val itemSection = itemSection.getConfigurationSection(key)!!
                    val material = itemSection.getString("material")!!
                    val name = itemSection.getString("displayName")
                    val click = itemSection.getString("clickAction")
                    val amount = itemSection.getInt("amount")
                    val slot = itemSection.getInt("slot")
                    val flags = itemSection.getList("itemFlags") as List<String>?
                    val enchants = itemSection.getList("enchantments") as List<String>?
                    val lores = itemSection.getList("lores") as List<String>?
                    val builder = ItemBuilder(Material.valueOf(material.uppercase()), if(amount <= 0) 1 else amount, if(name.isNullOrEmpty()) "<reset>" else name)
                    val customLores = LinkedList<String>()

                    if(enchants != null)
                        for(enchant in enchants) {
                            val encSplit = enchant.split(":")
                            builder.enchantments[Enchantment.getByName(encSplit[0])!!] = encSplit[1].toInt()
                        }

                    if(flags != null)
                        for(flag in flags) builder.itemFlags.add(ItemFlag.valueOf(flag))

                    if(lores != null) customLores.addAll(lores.toTypedArray().toList())

                    if(customLores.isNotEmpty()) builder.lores = customLores

                    val itemStack = builder.build()

                    if(key.equals("placeholder", false)) {
                        invBuilder.placeholder(builder.build())
                        continue
                    }

                    if(click != null) {
                        val nsKey = NamespacedKey(JavaPlugin.getProvidingPlugin(SimpleGUICreator::class.java), "custom-redirect")
                        val meta = itemStack.itemMeta

                        meta.persistentDataContainer.set(nsKey, PersistentDataType.STRING, click.replace("{", "").replace("}", ""))
                        itemStack.itemMeta = meta
                    }

                    val invItem = InventoryItem(itemStack, slot, {
                        if(click != null) {

                            if(redirectPattern.matcher(click).matches()) {
                                val inv = manager.getInventory(subDirectory, it.currentItem!!)

                                it.whoClicked.openInventory(inv!!)
                                return@InventoryItem
                            }

                            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), click.replace("%p", it.whoClicked.name))
                        }
                    })

                    invBuilder.addItem(invItem)
                }
            }
            invs[file.name.replace(".yml", "")] = invBuilder
        }

        manager.addInventory(CommandGUIs(subDirectory, invs))
    }
}