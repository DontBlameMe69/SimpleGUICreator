package dev.dontblameme.sgc.utils

import dev.dontblameme.sgc.main.SimpleGUICreator
import org.bukkit.NamespacedKey
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack
import org.bukkit.persistence.PersistentDataType
import org.bukkit.plugin.java.JavaPlugin
import java.util.*

/**
 * Creator: DontBlameMe69
 * Date: Mar 11, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

class CustomInventoryManager() {

    private val invs = LinkedList<CommandGUIs>()

    fun addInventory(cmdGUI: CommandGUIs) = invs.add(cmdGUI)

    fun getInventory(folder: String, item: ItemStack): Inventory? {
        if(!item.hasItemMeta()) return null

        val nsKey = NamespacedKey(JavaPlugin.getProvidingPlugin(SimpleGUICreator::class.java), "custom-redirect")

        if(!item.itemMeta.persistentDataContainer.has(nsKey)) return null

        val redirect = item.itemMeta.persistentDataContainer.get(nsKey, PersistentDataType.STRING)!!

        for(cmd in invs) {
            if(folder != cmd.dir) continue

            for((key, value) in cmd.builder)
                if(key== redirect)
                    return value.build()
            }

        return null
    }

    fun getInventory(folder: String, fileName: String): Inventory? {
        for(cmd in invs) {
            if(folder != cmd.dir) continue

            for((key, value) in cmd.builder)
                if(key == fileName)
                    return value.build()
        }
        return null
    }
}